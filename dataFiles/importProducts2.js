var xlsx = require('xlsx');
var workbook = xlsx.readFile('productsFromSales.xlsx');
var async = require('async');
var Promise = require("bluebird");

//console.log(workbook);
var mysql = require('mysql');
//var connection = mysql.createConnection({
var pool = mysql.createPool({
    connectionLimit : 10,
    host     : 'localhost',
    user     : 'root',
    password : '',
    database : 'onecost'
});

pool.on('enqueue', function () {
  console.log('Waiting for available connection slot');
});


//var itemsList = readMyData;
var itemsToDb = [],
    itemList = [];



runMyApp();

//main bit
function runMyApp() { 
async.waterfall([
    readMyData,
    addSupplierId,
    getNewProductsFromList
], function (error) {
    if (error) {
        //handle readFile error or processFile error here
        console.log(itemsToDb);
    }
});
    
}


function readMyData() {
    var sheet_name_list = workbook.SheetNames;
    var counter = 0,
        alreadyInCounter = 0,
        supplier,
        productCode;
        
    
    sheet_name_list.forEach(function(y) {
        var worksheet = workbook.Sheets[y];
        for (z in worksheet) {
            if(z[0] === '!') continue;
            counter++;
            if (counter === 1) {
                supplier = JSON.stringify(worksheet[z].v);
            }

            if (counter === 2) {
                counter = 0;
                productCode = JSON.stringify(worksheet[z].v);
                var product = {code : productCode, supplier : supplier};
                if (!itemList.indexOf(product)) {
                    console.log('Added product: ' + product);
                    //products.push(product);
                    itemList.push(product);
                }else{
                    alreadyInCounter++;
                }
            }
        }
        
    });
    //callback(true);
    return itemList;
}


function addSupplierId() {
    itemsList.forEach(function(product){
        product.sid = getSupplierId(product.supplier);
    });
    
}

function getNewProductsFromList(callback) {
    itemsList.forEach(function(product){
        if (result = getProductId(product.code, product.sid)) {
            console.log('Result from getNewProduct: ');
            console.log(result);
            if (result.productId !== null) {
                itemsToDb.push(product);   
            }
        }
        
    });
}

function addProduct(product, callback) {
    var result = result;
    console.log('Trying to write a product!');    
    var sql = 'insert into product (productCode, supplier_id) select ' + product.code + ', ' + product.sid;
    pool.getConnection(function(err, connection) {
        if (err) throw err;
        connection.query(sql, function(err, results) {
            if (err) throw err;
            //console.log(results);
            connection.release();
            callback(results);
        });
    });
};

function getSupplierId(name, callback) {
    var rows = rows;
    console.log('Searching for Supplier ID.');
    var sql = 'SELECT id FROM supplier WHERE name = ' + name + ' LIMIT 1';
    pool.getConnection(function(err, connection) {
        if (err) throw err;
        connection.query(sql, function(err, rows) {
            if (err) throw err;
            //console.log(rows);
            connection.release(); 
            callback({supplierId : rows[0].id});
        });
    });  
};

function getProductId (code, sid, callback) {
    console.log('Searching for Product ID.');
    var rows = rows;
     var sql = 'SELECT id FROM product WHERE productCode = ' + code + ' AND supplier_id = ' + sid;
     pool.getConnection(function(err, connection) {
        if (err) throw err;
        connection.query(sql, function(err, rows) {
            if (err) throw err;
            //console.log(rows);
            connection.release();
            if (rows.length > 0){
                callback({productId : rows[0].id});
            }else {
                callback({productId : null});
            }
        });
    });  
}



