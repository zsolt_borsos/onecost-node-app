var xlsx = require('xlsx');
var mysql = require('mysql');

//var connection = mysql.createConnection({
//creating a pool for db connection management
var pool = mysql.createPool({
    connectionLimit : 50,
    host     : 'localhost',
    user     : 'root',
    password : '',
    database : 'onecost'
});

//message if there are too many open connections
pool.on('enqueue', function () {
  console.log('Waiting for available connection slot');
});


//
//  DATABASE FUNCTIONS
//

//GET FUNCTIONS

function getAllProductDetailsFromDb(callback) {
    console.log('Getting All Products from DB...');
    var sql = 'SELECT p.productCode AS Productcode, pd.description AS Description, pd.packsize AS Packsize, s.name AS Supplier FROM product AS p LEFT JOIN productDetails AS pd ON p.id = pd.productId JOIN supplier AS s ON s.id = p.supplierId';
    pool.getConnection(function(err, connection) {
        if (err) throw err;
        connection.query(sql, function(err, rows) {
            if (err) throw err;
            //console.log(rows);
            connection.release();
            callback(rows);
        });
    });
}


function getAllSupplierDetailsFromDb(callback) {
    console.log('Getting All Suppliers from DB...');
    var sql = 'SELECT name AS Supplier FROM supplier';
    pool.getConnection(function(err, connection) {
        if (err) throw err;
        connection.query(sql, function(err, rows) {
            if (err) throw err;
            //console.log(rows);
            connection.release();
            callback(rows);
        });
    });
}

function getAllClientDetailsFromDb(callback) {
    console.log('Getting All Clients from DB...');
    var sql = 'SELECT name AS Client FROM client';
    pool.getConnection(function(err, connection) {
        if (err) throw err;
        connection.query(sql, function(err, rows) {
            if (err) throw err;
            //console.log(rows);
            connection.release();
            callback(rows);
        });
    });
}


//ADD RECORDS IN DB FUNCTIONS

function addProduct(product, callback) {
    if (!product) callback(0);
    var code = product.Productcode;
    if (code.charAt(0) === '"') {
        code = code.substring(1, code.length-1);   
    }
    //console.log('Adding to bd: ' + code);
    //console.log('Trying to write a product!');    
    var sql = 'INSERT INTO product (productCode, supplierId) SELECT ?, (SELECT id FROM supplier WHERE name = \'' + product.Supplier + '\')';
    pool.getConnection(function(err, connection) {
        if (err) {
         console.log(err);
         throw err;   
        }
        connection.query(sql,[code], function(err, results) {
            if (err) {
                console.error('Not good!');
                console.log(sql);
                console.log(code);
                console.log(err);
                throw err;   
            }
            
            //console.log(results);
            //console.log("1st part is good in product entering stuff!");
            
            var sqlDesc = 'INSERT INTO productDetails (description, packsize, productId) SELECT ?, ?, ?';
            connection.query(sqlDesc, [product.Description, product.Packsize, results.insertId], function(err, result) {
            
                //console.log(results);
                connection.release();
                callback(result);
            });
        });
    });
};

//add a client in the db
//expecting a string as 'client'
function addClient(client, callback) {
    
    if (!client) callback(0);
    
    var sql = 'INSERT INTO client (name) SELECT ?';
    
    pool.getConnection(function(err, connection) {
        if (err) {
            console.log(err);
            throw err;   
        }
        connection.query(sql,[client], function(err, results) {
            if (err) {
                console.error('Not good!');
                console.log(sql);
                console.log(code);
                console.log(err);
                throw err;   
            }
            connection.release();
            callback(results);
        });
    });    
}


function addSupplier(supplier, callback) {
    
    if (!supplier) callback(0);
    
    var sql = 'INSERT INTO supplier (name) SELECT ?';
    
    pool.getConnection(function(err, connection) {
        if (err) {
            console.log(err);
            throw err;   
        }
        connection.query(sql,[supplier], function(err, results) {
            if (err) {
                console.error('Not good!');
                console.log(sql);
                console.log(code);
                console.log(err);
                throw err;   
            }
            connection.release();
            callback(results);
        });
    });  
}

//
// RECURSIVE FUNCTIONS FOR DATA ENTRY
//

function addProductRecursive(products, i, callback) {
    //exit/finish rule
    if (i === products.length) {
        callback(i);
        return i;
    }
    //add the product using the async DB function
    addProduct(products[i], function(done){
        //increment counter
        i++;
        //recursively call itself passing in the counter
        addProductRecursive(products, i, callback);
    });
}


function addClientsRecursive(clients, i, callback) {
    
    if (i === clients.length) {
        callback(i);
        return i;
    }
    
    //add clients here
    addClient(clients[i], function(done) {
        i++;
        addClientsRecursive(clients, i, callback);        
    });
}

function addSupplierRecursive(suppliers, i, callback) {
 
    if (i === suppliers.length) {
        callback(i);   
        return i;
    }
    
    addSupplier(suppliers[i], function(done){
        i++;
        addSupplierRecursive(suppliers, i, callback);
    });
}



//
// APP LOGIC FUNCTIONS
//

/*
plan: 
- check supplisers / add new suppliers
- check clients / add new clients
- check products / add new products
- add sales /check existing sales info for the date?? maybe...

*/

//read the file and turn it into json row by row using 1st row as object properties
function readFileData(filename) {
    
    var workbook = xlsx.readFile(filename);
    var sheet_name_list = workbook.SheetNames;
    //creating array for products
    var salesInfo = [];
        
    //iterate list of sheets/allow to impoert multiple worksheets? maybe future feature
    sheet_name_list.forEach(function(y) {
        var worksheet = workbook.Sheets[y];
        salesInfo = xlsx.utils.sheet_to_json(worksheet);
    });
    return salesInfo;
} 


//
// GET UNIQUE LISTS FOR INSERTION/CHECK
//

//get the list of unique suppliers from sales list
function getSupplierList(list) {
    var suppliers = [];
    for (var i = 0; i < list.length; i++) {
        var alreadyIn = false;
        for (var k = 0; k < suppliers.length; k++) {
            if (list[i].Supplier === suppliers[k]) {
                alreadyIn = true;
                break;
            }
        }
        if (!alreadyIn) {
            suppliers.push(list[i].Supplier);
        }
    }
    return suppliers;
}

//get the list of unique clients from sales list
function getClientList(list) {
    var clients = [];
    for (var i = 0; i < list.length; i++) {
        var alreadyIn = false;
        for (var k = 0; k < clients.length; k++) {
            if (list[i].Client === clients[k]) {
                alreadyIn = true;
                break;
            }
        }
        if (!alreadyIn) {
            clients.push(list[i].Client);
        }
    }
    return clients;
}

//get the list of unique products from sales list
function getProductList(list) {
    var uniqueProducts = [];
    for (var i = 0; i < list.length; i++) {
        var product = list[i],
            alreadyIn = false;
        for (var k = 0; k < uniqueProducts.length; k++) {
            if ((product.Productcode === uniqueProducts[k].Productcode) && (product.Supplier === uniqueProducts[k].Supplier)) {
                alreadyIn = true;
                break;
            }
        }
        if (!alreadyIn) {
            uniqueProducts.push(product);
        }
    }
    return uniqueProducts;
}

//
// END
//


function getInfoFromFile(callback) {
    sales = readFileData('ocsales.xlsx');
    //sales = readFileData('suppliers/Brakes Frozen.xlsx');
    //sales = readFileData('salesTester.xlsx');
    suppliers = getSupplierList(sales);
    clients = getClientList(sales);
    products = getProductList(sales);
    //console.log(suppliers);
    //console.log(clients);
    //console.log(products); 
    //console.log(sales);
    callback(true);   
}

//get everything from DB. This will need to be tuned as products will be huge!!
function getInfoFromDb(callback) {
    getAllClientDetailsFromDb(function(result){
        dbClients = result;
        getAllSupplierDetailsFromDb(function(result){
            dbSuppliers = result;
            getAllProductDetailsFromDb(function(result){
                dbProducts = result;
                callback(true);
            });
        });
    });   
}

//check items -> get new items if any
//create a list of clients that are not in the db
function createClientListToDb(clientsInFile, clientsInDb) {
    
    //create array
    var clients = [];
    //console.log(clientsInDb);
    
    //checking if clients are in db
    clientsInFile.forEach(function(client){
        //console.log(client);
        
        //set flag as false
        var found = false;
        
        //iterate clients in db
        clientsInDb.forEach(function(clientDb){
            
            //check if client is (actually) in db
            if (clientDb.Client == client){
                //console.log("Found a client in db: " + client);
                //set flag as true
                found = true;
                //no need to continue, return.
                return;
            }
            
        });
        
        //check if flag is set
        if (!found) {
            // if client is not in then add it as new
            clients.push(client);   
        }
        
    });
    //return the array
    return clients;
}


//create a list of suppliers that are not in the db
function createSupplierListToDb(suppliersInFile, suppliersInDb){
    
    //create array
    var suppliers = [];
    
    //iterate suppliers (in file)
    suppliersInFile.forEach(function(supplier){
        
        //set flag as false by default
        var found = false;
        //console.log("Supplier in file: " + supplier);
        
        //iterate suppliers in db
        suppliersInDb.forEach(function(supplierDb){
            //console.log("Supplier in db: ");
            //console.log(supplierDb);
            if (supplierDb.Supplier == supplier) {
                found = true;
                //return if found
                return;
            }
        });
        
        if (!found){
            //add supplier to array if not found in db
            suppliers.push(supplier);   
        }
        
    });
    //return the array
    return suppliers;
}

//create the products list for inserting to the db
function createProductListToDb(productsInFile, productsInDb){

    var products = [];
    
    productsInFile.forEach(function(product){
    
        var found = false;
        //console.log(product);
        productsInDb.forEach(function(productDb){
        
            
            if ((productDb.Supplier == product.Supplier) && (productDb.Productcode == product.Productcode)) {
                found = true;
                return;
            }
        });
        
        if (!found) {
            products.push(product);   
        }
        
    });
    return products;
}

function createInsertLists() {
    
    //clients
    newClients = createClientListToDb(clients, dbClients);
    
    if (newClients.length > 0){
        console.log("Found " + newClients.length + " new client(s) in this file.");    
    }else{
        console.log("All clients are in the db already.");   
    }
    
    //suppliers
    newSuppliers = createSupplierListToDb(suppliers, dbSuppliers);
    
    if (newSuppliers.length > 0) {
        console.log("Found " + newSuppliers.length + " new suppliers in this file.");   
    }else {
        console.log("All suppliers are in the db already.");   
    }
    
    //products
    newProducts = createProductListToDb(products, dbProducts);
    
    if (newProducts.length > 0) {
        console.log("Found " + newProducts.length + " new products in this file.");
    }else {
        console.log("All products are already in the db.");   
    }
    
}


//functions to call the recursive functions... 

function enterNewProductsInDb(callback) {
    
    if (newProducts.length > 0) {
        addProductRecursive(newProducts, 0, function(counter){
            callback(counter);
        });
    }else {
        callback(0);   
    }
}

function enterNewClientsInDb(callback) {
    
    if (newClients.length > 0) {
        addClientsRecursive(newClients, 0, function(counter){
            callback(counter);
        });
    }else {
        callback(0);   
    }
}

function enterNewSuppliersInDb(callback) {
    
    if (newSuppliers.length > 0) {
        addSupplierRecursive(newSuppliers, 0, function(counter){
            callback(counter);
        });   
    }else {
        callback(0);   
    }
    
}


// main app function

//variables storing document data
var sales, suppliers, clients, products;

//variables storing db data
var dbSuppliers, dbClients, dbProducts;

//variables for inserting data
var newClients, newSuppliers, newProducts;

function runMyApp(callback){
    getInfoFromFile(function(result){
        console.log('\nFile read successfully.');
        console.log('Clients found: ' + clients.length);
        console.log('Suppliers found: ' + suppliers.length);
        console.log('Unique Products in list: ' + products.length);
        console.log(sales.length + ' record found in file.\n');
        
    });
    
    getInfoFromDb(function(result){
        
        createInsertLists();
        
        enterNewSuppliersInDb(function(sCounter){
            console.log("Added " + sCounter + " suppliers.")
            enterNewClientsInDb(function(cCounter){
                console.log("Added " + cCounter + " clients.");
                enterNewProductsInDb(function(pCounter){
                    console.log("Added " + pCounter + " products.");
                    callback(true);
                });    
            });
        });
    });    
}


//run the import app
runMyApp(function(result){
    pool.end();
    console.log('Finished app.');
    
});
