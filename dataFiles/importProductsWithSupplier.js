var xlsx = require('xlsx');
var mysql = require('mysql');

//var connection = mysql.createConnection({
//creating a pool for db connection management
var pool = mysql.createPool({
    connectionLimit : 99,
    host     : 'localhost',
    user     : 'root',
    password : '',
    database : 'onecost'
});

//benchmark
var myTime = new Date(),
    startTime;

//message if there are too many open connections
pool.on('enqueue', function () {
  console.log('Waiting for available connection slot');
});

var excludedProducts = [],
    productsWithoutSupplier = [];


//
// DATABASE FUNCTIONS
//

function addProduct(product, callback) {
    if (!product) return;
    var code = product.code;
    if (code.charAt(0) === '"') {
        code = code.substring(1, code.length-1);   
    }
    //console.log('Adding to bd: ' + code);
    //console.log('Trying to write a product!');    
    var sql = 'insert into product (productCode, supplierId) select ?, ' + product.sid;
    pool.getConnection(function(err, connection) {
        if (err) {
         console.log(err);
         throw err;   
        }
        connection.query(sql,[code], function(err, results) {
            if (err) {
                console.error('Not good!');
                console.log(sql);
                console.log(code);
                console.log(err);
                throw err;   
            }
            //console.log(results);
            connection.release();
            callback(results);
        });
    });
};


function getAllSupplierDetails(callback) {
    console.log('Getting All Supplier details');
    var sql = 'SELECT * FROM supplier';
    pool.getConnection(function(err, connection) {
        if (err) throw err;
        connection.query(sql, function(err, rows) {
            if (err) throw err;
            //console.log(rows);
            connection.release();
            callback(rows);
        });
    });
}


function getAllProductsDetails(callback) {
    console.log('Getting All Product details');
    var sql = 'SELECT * FROM product';
    pool.getConnection(function(err, connection) {
        if (err) throw err;
        connection.query(sql, function(err, rows) {
            if (err) throw err;
            //console.log(rows);
            connection.release();
            callback(rows);
        });
    });
}



//
//  DATABASE FUNCTIONS END
//


//
//  APPLICATION LOGIC FUNCTIONS
//


//read data from excel file
function readMyData(filename) {
    
    var workbook = xlsx.readFile(filename);
    
    var sheet_name_list = workbook.SheetNames;
    var addedCounter = 0,
        alreadyInCounter = 0,
        alreadyIn,
        supplier,
        productCode,
        products = [],
        skipped = [];
        
    //iterate list of sheets
    sheet_name_list.forEach(function(y) {
        var worksheet = workbook.Sheets[y];
        
        //counter for columns
        var counter = 0;
        //iterate rows
        for (z in worksheet) {
            if(z[0] === '!') continue;
            counter++;
            //get 1st column value -> supplier name
            if (counter === 1) {
                supplier = JSON.stringify(worksheet[z].v);
                if (!supplier) {
                    console.log('No supplier name from file on line: ' + z);   
                }
            }
            //get 2nd column value -> product code
            if (counter === 2) {
                //reset counter for next row
                counter = 0;
                //set alreadyIn flag
                alreadyIn = false;
                //get value of productCode
                productCode = JSON.stringify(worksheet[z].v);
                if (!productCode) {
                    console.log('No product code from excel file on line: ' + z);   
                }
                //create product object
                var product = {code : productCode, supplier : supplier};
                //using the product object check for existance in the products list
                for (var i = 0; i < products.length; i++) {
                    //check for similarity
                    if ((products[i].code === product.code) && (product.supplier === products[i].supplier)) {
                        //flag similarity
                        alreadyIn = true;
                        break;
                    }
                }
                
                //add product if it is not in the list
                if (!alreadyIn) {
                    //console.log('Added product: ' + productCode);
                    
                    //record added products
                    addedCounter++;
                    products.push(product);
                }else{
                    //console.log('Not adding this product ' + productCode);
                    
                    //record skipped products
                    skipped.push(product);
                    alreadyInCounter++;
                }
           
            }  //end of if (2)
            
            //debug: add hardcoded limit to product list 
            //if (addedCounter == 2000) break;
            
        }  //end of for
        
    });
    
    //print out some information
    //could improve this, not many tracked information is used!
    console.log('Unique products: ' + addedCounter);
    console.log('Duplications in this list: ' + alreadyInCounter);
    //return the list of unique products from the read file
    return products;
}


//the function modifies the passed in products list -> adds sid (supplier ID) to each object
function addSidToProductList(products, suppliers) {
    //console.log(suppliers);
    //iterate product list
    for (var i = 0; i < products.length; i++) {
        //use variable for conveniance
        var sname = products[i].supplier;
        //console.log('Supplier name (uncleaned): ' + sname);
        // clean string, get rid of "s
        if (String(sname).charAt(0) == '"') {
            sname = sname.substring(1, (sname.length-1));
        }
        //console.log(sname);
        //iterate supplier lists
        for (var k = 0; k < suppliers.length; k++) {
            //console.log('Supplier details:');
            //console.log(suppliers[k].name);
            
            //check if this product's supplier is the current one
            if (sname == suppliers[k].name){
                //if the name is a match then add this object's id to the product as well
                products[i].sid = suppliers[k].id; 
                //no need for further check, break out from nested loop
                break;
            }
        }   
    }
}

//filter the newProducts list using the productsInDb to avoid product duplications in the DB 
function getNewProductsList(newProducts, productsInDb) {
    //console.log(productsInDb);
    //return the full list if there are no products in product table
    if (productsInDb.length === 0) return newProducts;
    
    var products = [];
    
    for (var i = 0; i < newProducts.length; i++) {
        //console.log(newProducts[i]);
        
        var code = String(newProducts[i].code),
            sid = newProducts[i].sid,
            alreadyIn = false;
        
        //clean up code
        //console.log(code);
        if (code.charAt(0) == '"') {
            code = code.substring(1,(code.length-1));   
            //console.log(code);
        }
        
        //clean up sid
        //console.log(sid);
        if (String(sid).charAt(0) == '"') {
            sid = sid.substring(1, (sid.length-1));   
            //console.log(sid);
        }
        
        //iterate list 
        for (var k = 0; k < productsInDb.length; k++) {
            var kcode = String(productsInDb[k].productCode),
                ksid = productsInDb[k].supplierId;
            //console.log(kcode);
            
            //safe clean kcode
            if (kcode.charAt(0) == '"') {
                kcode = kcode.substring(1, (kcode.length-1));   
                //console.log(kcode);
            }
            
            //safe clean ksid
            if (String(ksid).charAt(0) == '"') {
                ksid = ksid.substring(1, (ksid.length-1));   
            }
            
            //check if code and sid is the same
            if ((kcode == code) && (ksid == sid)){
                //console.log('Already in db! -> ' + kcode); 
                //if it is the same, then change alreadyIn to flag this
                alreadyIn = true;
                //no need for further checking, break out from this loop
                break;
            }
            
        } //end of nested for
        //check for alreadyIn flag
        if (!alreadyIn) {
            //console.log('Adding product to list (to db): ');
            //console.log(newProducts[i]);
            
            //push new product if it is actually new
            products.push(newProducts[i]);
            
        }
    }
    return products;
}


// logic to create the list using multiple functions
function createListToDb(callback){
    //get the unique list from file
    
    /*
    files used:
    'productsFromSalesOrig.xlsx'
    'productsFromSales.xlsx'
    't1.xlsx'
    
    */
    var newProducts = readMyData('productsFromSalesOrig.xlsx', {});
    //console.log(products);
    
    //get suppliser details from DB
    getAllSupplierDetails(function(result){
        
        //combine lists -> add supplier ID to the unique product list
        addSidToProductList(newProducts, result);
        //console.log(result);
        
        //get products from DB 
        getAllProductsDetails(function(result){
            //filter out products from the list that is already in the DB
            var productsToDb = getNewProductsList(newProducts, result);
            
            console.log('Products in DB list length: ' + result.length);
            //return the list of products that will be added to the DB
            callback(productsToDb);
        });
    });
}


//a recursive call to DB to avoid async calls
//this function makes it possible to make multiple BD calls without using any Promise or async library
//this might be a hack, research needed... 
//but for now it works perfectly
function addProductRecursive(products, i, callback) {
    //exit/finish rule
    if (i === products.length) {
        callback(i);
        return true;
    }
    //add the product using the async DB function
    addProduct(products[i], function(done){
        //increment counter
        i++;
        //recursively call itself passing in the counter
        addProductRecursive(products, i, callback);
    });
}



// main app logic
function startApp(callback){
    startTime = myTime.getTime();
    //create the list for the DB
    createListToDb(function(list){
        var i = 0;
        //return if nothing to add
        if (list.length === 0 ) {
            callback('Nothing to add.');
            return;
        }
        //console.log('Adding product to DB:');
        console.log('Preparing to add ' + list.length + ' products to DB.');
        //console.log(list);
        
        //add the products to the DB
        addProductRecursive(list, 0, function(done){
            console.log('Added ' + done + ' products to DB.');
            callback('Success.');
        });
        
    });
}


// start the application

startApp(function(res){
    if (res) {
        console.log(res);
        //end the mysql connection pool
        pool.end(); 
        //var endTime = myTime.getTime(),
        //    elapsed = endTime - startTime;
        
        //console.log('This import took ' + elapsed.toString() + 's to run. Hurray!');
    }
});
