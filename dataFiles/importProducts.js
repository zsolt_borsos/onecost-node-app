var xlsx = require('xlsx');
var workbook = xlsx.readFile('productsFromSales.xlsx');
var async = require('async');

//console.log(workbook);
var mysql      = require('mysql');
//var connection = mysql.createConnection({
var pool  = mysql.createPool({
    connectionLimit : 10,
    host     : 'localhost',
    user     : 'root',
    password : '',
    database : 'onecost'
});



//connection.connect();
var sheet_name_list = workbook.SheetNames;
var productError = 0,
    success = 0,
    alreadyInCounter = 0,
    cycleCounter = 0,
    productsToEnter = [],
    addingTheSame = 0;

sheet_name_list.forEach(function(y) {
    var counter = 0,
    supplier, 
    productCode;
    var worksheet = workbook.Sheets[y];
     
    for (z in worksheet) {
        if(z[0] === '!') continue;
        counter++;
        cycleCounter++;
        //console.log(z);
    //console.log(y + "!" + z + "=" + JSON.stringify(worksheet[z].v));
        if (counter === 1) {
            supplier = JSON.stringify(worksheet[z].v);
            //supplier = mysql.escape(supplier);
            //console.log('Supplier:' + supplier);
        }
        
        if (counter === 2) {
            counter = 0;
            productCode = JSON.stringify(worksheet[z].v);
            //productCode = mysql.escape(productCode);
            //console.log('Product Code: ' + productCode);
            //query
            //pre: get supplier id /add supplier?
            //1.check if product is already in db
            //2.if it is a new product then add it
            
            
            /* this needs fine tuning
            var checker = 0;
            var product = {productCode : productCode, supplier : supplier};
            for (p in productsToEnter) {
                if (p === product)
                    checker++;
            }
            if (checker === 0) {
                productsToEnter.push(product);
            
                inTheDB(productCode, supplier);   
            }else{
                addingTheSame++;   
            }
            */
            
            inTheDB(productCode, supplier);   
            
            /*
            async.series([
                function(callback){
                    getSupplierId(supplier, function(result){
                    console.log(result);
                    if (sid = result.supplierId) {
                        console.log('got a sid');
                    }
                    callback(null, sid);
                },
                function(callback){
                    
                    callback(null, 'two');
                }
            ],
            // optional callback
            function(err, results){
                // results is now equal to ['one', 'two']
            });
            */
            /*
            getSupplierId(supplier, function(result){
                console.log(result);
                if (sid = result.supplierId) {
                    console.log('got a sid');
                    //check for product
                    getProductId(productCode, sid, function(result){
                        console.log(result);
                        if (!result.productId) {
                            console.log('got a pid');
    
                            addProduct({code : productCode, sid : sid}, function(done){
                                console.log(done);
                            }); 
                        }
                    });
                }
            });
            */
            
        }  //end of if
       // if (cycleCounter > 150) break;
    } //end of for
    
    //console.log('Tried to add existing products ' +addingTheSame + ' times.');
});

pool.on('enqueue', function () {
  console.log('Waiting for available connection slot');
});


function inTheDB(productCode, supplier) {
   
    async.waterfall([
                
                function(callback){
                    var sid, pid;
                    pool.getConnection(function(err, connection) {
                        var sql = 'SELECT id FROM supplier WHERE name = ? LIMIT 1';
                        var query = connection.query(sql, [supplier]);
                        query.on('result', function(rows){
                            console.log('Supplier: ');
                            console.log(rows);
                            if (!sid = rows.id) sid = null;
                            
                        })
                        .on('end', function(){
                            connection.release();
                            callback(null, sid);
                            
                        });
                    });
                    
                },
                function(sid, callback){
                    var pid;
                    if (sid) {
                        pool.getConnection(function(err, connection) {
                            var sql = 'SELECT id FROM product WHERE productCode = ? AND supplier_id = ? LIMIT 1';
                            var query = connection.query(sql, [productCode, sid]);
                            query.on('result', function(rows){
                                console.log('Product: ');
                                console.log(rows);
                                if (!pid = rows.id) pid = null;

                            })
                            .on('end', function(){
                                connection.release();
                                callback(null, pid, sid);
                            });
                        });
                    }
                
                },
                function(pid, sid, callback){
                    var result;
                    if (pid === null) {
                    pool.getConnection(function(err, connection) {
                        if (err) throw err;
                        var sql = 'insert into product (productCode, supplier_id) VALUES (?, ?)';
                        var query = connection.query(sql, [productCode, sid]);
                        query.on('result', function(result){
                            var done = result;
                            
                        })
                        .on('end', function(){
                            connection.release();
                            callback(null, result);
                        });
                    });
                    }else{
                        //connection.release();
                        //alreadyInCounter++;
                        callback(null, -1);
                    }             
                }
            ], function (err, result) {
               // result now equals 'done' 
                if (err) throw err;
                if (result === -1) alreadyInCounter++;
                console.log('Done.');
                console.log('Already in: ' + alreadyInCounter);
                console.log('All: ' + cycleCounter / 2 );
                console.log('Results: ');
                console.log(result);
                
            });
        
        
 
}





function addProduct(product) {
    //var result = result;
    console.log('Trying to write a product!');    
    var sql = 'insert into product (productCode, supplier_id) select ' + product.code + ', ' + product.sid;
    pool.getConnection(function(err, connection) {
        if (err) throw err;
        connection.query(sql, function(err, results) {
            if (err) throw err;
            //console.log(results);
            connection.release();
            //callback(results);
            return results;
        });
    });
};

function getSupplierId(name) {
    //var rows = rows;
    console.log('Searching for Supplier ID.');
    var sql = 'SELECT id FROM supplier WHERE name = ' + name + ' LIMIT 1';
    pool.getConnection(function(err, connection) {
        if (err) throw err;
        connection.query(sql, function(err, rows) {
            if (err) throw err;
            //console.log(rows);
            connection.release(); 
            //callback({supplierId : rows[0].id});
            return rows[0].id;
        });
    });  
};

function getProductId (code, sid) {
    console.log('Searching for Product ID.');
    //var rows = rows;
     var sql = 'SELECT id FROM product WHERE productCode = ' + code + ' AND supplier_id = ' + sid;
     pool.getConnection(function(err, connection) {
        if (err) throw err;
        connection.query(sql, function(err, rows) {
            if (err) throw err;
            //console.log(rows);
            connection.release();
            if (rows.length > 0){
                //callback({productId : rows[0].id});
                return rows[0].id;
            }else {
                //callback({productId : null});
                return null;
            }
        });
    });  
}



/*

//callback versions

function addProduct(product, callback) {
    var result = result;
    console.log('Trying to write a product!');    
    var sql = 'insert into product (productCode, supplier_id) select ' + product.code + ', ' + product.sid;
    pool.getConnection(function(err, connection) {
        if (err) throw err;
        connection.query(sql, function(err, results) {
            if (err) throw err;
            //console.log(results);
            connection.release();
            callback(results);
        });
    });
};

function getSupplierId(name, callback) {
    var rows = rows;
    console.log('Searching for Supplier ID.');
    var sql = 'SELECT id FROM supplier WHERE name = ' + name + ' LIMIT 1';
    pool.getConnection(function(err, connection) {
        if (err) throw err;
        connection.query(sql, function(err, rows) {
            if (err) throw err;
            //console.log(rows);
            connection.release(); 
            callback({supplierId : rows[0].id});
        });
    });  
};

function getProductId (code, sid, callback) {
    console.log('Searching for Product ID.');
    var rows = rows;
     var sql = 'SELECT id FROM product WHERE productCode = ' + code + ' AND supplier_id = ' + sid;
     pool.getConnection(function(err, connection) {
        if (err) throw err;
        connection.query(sql, function(err, rows) {
            if (err) throw err;
            //console.log(rows);
            connection.release();
            if (rows.length > 0){
                callback({productId : rows[0].id});
            }else {
                callback({productId : null});
            }
        });
    });  
}
*/