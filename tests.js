var request = require('supertest');
var app = require('./app');

describe('Request to the root path', function(){
    
    it('Returns a 200 status code', function(done){
        request(app)
            .get('/')
            .expect(200)
            .end(function(error){
                if (error) throw error;
                done();
            });        
    });
    
    it('Returns html format', function(done){
    
            request(app)
                .get('/')
                .expect('Content-Type', /html/, done);
    });
    
});


describe('Request to /api/import', function(){
    
    
    it('Can POST things', function(done){
        request(app)
            .post('/api/import')
            .send('stuff=go')
            .expect(201, done);
    });

});


