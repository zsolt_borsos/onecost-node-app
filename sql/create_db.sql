-- Created by Vertabelo (http://vertabelo.com)
-- Script type: create
-- Scope: [tables, references, sequences, views, procedures]
-- Generated at Wed Feb 11 11:14:55 UTC 2015




-- tables
-- Table accountManager
CREATE TABLE accountManager (
    id int    NOT NULL  AUTO_INCREMENT,
    name varchar(250)    NOT NULL ,
    email varchar(250)    NULL ,
    contact varchar(250)    NULL ,
    note varchar(250)    NULL ,
    supplierId int    NOT NULL ,
    CONSTRAINT id PRIMARY KEY (id)
);

-- Table area
CREATE TABLE area (
    id int    NOT NULL  AUTO_INCREMENT,
    name varchar(250)    NULL ,
    location varchar(250)    NOT NULL ,
    countyId int    NOT NULL ,
    note varchar(250)    NULL ,
    CONSTRAINT id PRIMARY KEY (id)
);

-- Table associate
CREATE TABLE associate (
    id int    NOT NULL  AUTO_INCREMENT,
    name varchar(250)    NOT NULL ,
    email varchar(250)    NULL ,
    mobile varchar(20)    NULL ,
    note varchar(250)    NULL ,
    associateTypeId int    NOT NULL ,
    CONSTRAINT id PRIMARY KEY (id)
);

-- Table associateReferredBy
CREATE TABLE associateReferredBy (
    associateId int    NOT NULL ,
    referredById int    NOT NULL ,
    value decimal(7,4)    NULL ,
    note varchar(250)    NULL ,
    CONSTRAINT id PRIMARY KEY (associateId,referredById)
);

-- Table associateType
CREATE TABLE associateType (
    id int    NOT NULL  AUTO_INCREMENT,
    name varchar(250)    NOT NULL ,
    payable bool    NOT NULL DEFAULT 0 ,
    CONSTRAINT id PRIMARY KEY (id)
);

-- Table cc
CREATE TABLE cc (
    id int    NOT NULL  AUTO_INCREMENT,
    date date    NOT NULL ,
    note text    NULL ,
    clientId int    NOT NULL ,
    CONSTRAINT cc_pk PRIMARY KEY (id)
);

-- Table client
CREATE TABLE client (
    id int    NOT NULL  AUTO_INCREMENT,
    name varchar(255)    NOT NULL ,
    accountNumber varchar(100)    NULL ,
    levelOfInterest int    NOT NULL DEFAULT 1 ,
    note varchar(250)    NULL ,
    clientGroupId int    NULL ,
    industryId int    NULL ,
    areaId int    NULL ,
    CONSTRAINT client_pk PRIMARY KEY (id)
);

-- Table clientAccountManager
CREATE TABLE clientAccountManager (
    clientId int    NOT NULL ,
    accountManagerId int    NOT NULL ,
    CONSTRAINT id PRIMARY KEY (clientId,accountManagerId)
);

-- Table clientGroup
CREATE TABLE clientGroup (
    id int    NOT NULL  AUTO_INCREMENT,
    name varchar(150)    NOT NULL ,
    note text    NULL ,
    CONSTRAINT clientGroup_pk PRIMARY KEY (id)
);

-- Table clientInSubdivisionWithSupplier
CREATE TABLE clientInSubdivisionWithSupplier (
    clientId int    NOT NULL ,
    supplierId int    NOT NULL ,
    subdivisionId int    NOT NULL ,
    CONSTRAINT clientInSubdivisionWithSupplier_pk PRIMARY KEY (clientId,supplierId)
);

-- Table clientRebate
CREATE TABLE clientRebate (
    rebateId int    NOT NULL ,
    clientId int    NOT NULL ,
    CONSTRAINT id PRIMARY KEY (rebateId,clientId)
);

-- Table clientReferral
CREATE TABLE clientReferral (
    id int    NOT NULL  AUTO_INCREMENT,
    value decimal(7,4)    NULL ,
    clientId int    NOT NULL ,
    note varchar(250)    NULL ,
    associateId int    NOT NULL ,
    CONSTRAINT id PRIMARY KEY (id)
);

-- Table clientStrategicArea
CREATE TABLE clientStrategicArea (
    strategicAreaId int    NOT NULL ,
    clientId int    NOT NULL ,
    CONSTRAINT id PRIMARY KEY (strategicAreaId,clientId)
);

-- Table county
CREATE TABLE county (
    id int    NOT NULL  AUTO_INCREMENT,
    name varchar(250)    NOT NULL ,
    CONSTRAINT id PRIMARY KEY (id)
);

-- Table industry
CREATE TABLE industry (
    id int    NOT NULL  AUTO_INCREMENT,
    name varchar(250)    NOT NULL ,
    note varchar(250)    NULL ,
    CONSTRAINT id PRIMARY KEY (id)
);

-- Table invoice
CREATE TABLE invoice (
    id int    NOT NULL  AUTO_INCREMENT,
    date date    NOT NULL ,
    imageName varchar(255)    NULL ,
    ccId int    NOT NULL ,
    note varchar(250)    NULL ,
    CONSTRAINT invoice_pk PRIMARY KEY (id)
);

-- Table invoiceItem
CREATE TABLE invoiceItem (
    id int    NOT NULL  AUTO_INCREMENT,
    invoiceId int    NOT NULL ,
    productId int    NOT NULL ,
    qty decimal(12,2)    NOT NULL ,
    note varchar(250)    NULL ,
    CONSTRAINT invoiceItem_pk PRIMARY KEY (id)
);

-- Table measureType
CREATE TABLE measureType (
    id int    NOT NULL  AUTO_INCREMENT,
    name varchar(150)    NOT NULL ,
    note varchar(250)    NULL ,
    CONSTRAINT measureType_pk PRIMARY KEY (id)
);

-- Table price
CREATE TABLE price (
    id int    NOT NULL  AUTO_INCREMENT,
    productId int    NOT NULL ,
    dateFrom date    NOT NULL ,
    dateTo date    NULL ,
    value decimal(12,2)    NOT NULL ,
    sourceTypeId int    NOT NULL ,
    sourceValue varchar(250)    NOT NULL ,
    note varchar(250)    NULL ,
    CONSTRAINT price_pk PRIMARY KEY (id)
);

-- Table product
CREATE TABLE product (
    id int    NOT NULL  AUTO_INCREMENT,
    productCode varchar(150)    NOT NULL ,
    productCategoryId int    NULL ,
    supplierId int    NOT NULL ,
    unitOfSale decimal(12,2)    NULL ,
    measureValue decimal(12,4)    NULL ,
    measureTypeId int    NULL ,
    added timestamp    NOT NULL ,
    note varchar(250)    NULL ,
    CONSTRAINT product_pk PRIMARY KEY (id)
);

-- Table productCategory
CREATE TABLE productCategory (
    id int    NOT NULL  AUTO_INCREMENT,
    name varchar(255)    NOT NULL ,
    productCategoryId int    NULL ,
    CONSTRAINT productCategory_pk PRIMARY KEY (id)
);

-- Table productDetail
CREATE TABLE productDetail (
    id int    NOT NULL  AUTO_INCREMENT,
    description varchar(255)    NOT NULL ,
    packsize varchar(255)    NULL ,
    productId int    NOT NULL ,
    `default` bool    NULL DEFAULT 0 ,
    CONSTRAINT productDetails_pk PRIMARY KEY (id)
);

-- Table productMatch
CREATE TABLE productMatch (
    invoiceItemId int    NOT NULL ,
    productId int    NOT NULL ,
    priceId int    NOT NULL ,
    qty decimal(12,2)    NOT NULL ,
    final bool    NULL ,
    note varchar(250)    NULL ,
    CONSTRAINT productMatch_pk PRIMARY KEY (invoiceItemId,productId)
);

-- Table purchase
CREATE TABLE purchase (
    purchaseId int    NOT NULL  AUTO_INCREMENT,
    date date    NOT NULL ,
    clientId int    NOT NULL ,
    note text    NULL ,
    CONSTRAINT purchase_pk PRIMARY KEY (purchaseId)
);

-- Table purchaseItem
CREATE TABLE purchaseItem (
    id int    NOT NULL  AUTO_INCREMENT,
    purchaseId int    NOT NULL ,
    qty decimal(12,4)    NOT NULL ,
    total decimal(12,2)    NOT NULL ,
    productId int    NOT NULL ,
    note varchar(250)    NULL ,
    CONSTRAINT purchaseItem_pk PRIMARY KEY (id)
);

-- Table rebate
CREATE TABLE rebate (
    id int    NOT NULL  AUTO_INCREMENT,
    value decimal(7,3)    NOT NULL ,
    dateFrom date    NOT NULL ,
    dateTo date    NULL ,
    productId int    NOT NULL ,
    note varchar(250)    NULL ,
    CONSTRAINT rebate_pk PRIMARY KEY (id)
);

-- Table sourceType
CREATE TABLE sourceType (
    id int    NOT NULL  AUTO_INCREMENT,
    name varchar(150)    NOT NULL ,
    note varchar(250)    NULL ,
    CONSTRAINT sourceType_pk PRIMARY KEY (id)
);

-- Table strategicArea
CREATE TABLE strategicArea (
    id int    NOT NULL  AUTO_INCREMENT,
    name varchar(250)    NOT NULL ,
    note varchar(250)    NULL ,
    CONSTRAINT id PRIMARY KEY (id)
);

-- Table subdivision
CREATE TABLE subdivision (
    id int    NOT NULL  AUTO_INCREMENT,
    name varchar(250)    NOT NULL ,
    CONSTRAINT subdivision_pk PRIMARY KEY (id)
);

-- Table supplier
CREATE TABLE supplier (
    id int    NOT NULL  AUTO_INCREMENT,
    name varchar(255)    NOT NULL ,
    note text    NULL ,
    defaultRebate decimal(10,2)    NULL ,
    CONSTRAINT supplier_pk PRIMARY KEY (id)
);

-- Table supplierArea
CREATE TABLE supplierArea (
    areaId int    NOT NULL ,
    supplierId int    NOT NULL ,
    CONSTRAINT id PRIMARY KEY (areaId,supplierId)
);





-- foreign keys
-- Reference:  accountManager_supplier (table: accountManager)


ALTER TABLE accountManager ADD CONSTRAINT accountManager_supplier FOREIGN KEY accountManager_supplier (supplierId)
    REFERENCES supplier (id);
-- Reference:  area_county (table: area)


ALTER TABLE area ADD CONSTRAINT area_county FOREIGN KEY area_county (countyId)
    REFERENCES county (id);
-- Reference:  associates_associateType (table: associate)


ALTER TABLE associate ADD CONSTRAINT associates_associateType FOREIGN KEY associates_associateType (associateTypeId)
    REFERENCES associateType (id);
-- Reference:  cc_client (table: cc)


ALTER TABLE cc ADD CONSTRAINT cc_client FOREIGN KEY cc_client (clientId)
    REFERENCES client (id);
-- Reference:  clientInSubdivisionWithSupplier_client (table: clientInSubdivisionWithSupplier)


ALTER TABLE clientInSubdivisionWithSupplier ADD CONSTRAINT clientInSubdivisionWithSupplier_client FOREIGN KEY clientInSubdivisionWithSupplier_client (clientId)
    REFERENCES client (id);
-- Reference:  clientInSubdivisionWithSupplier_subdivision (table: clientInSubdivisionWithSupplier)


ALTER TABLE clientInSubdivisionWithSupplier ADD CONSTRAINT clientInSubdivisionWithSupplier_subdivision FOREIGN KEY clientInSubdivisionWithSupplier_subdivision (subdivisionId)
    REFERENCES subdivision (id);
-- Reference:  clientInSubdivisionWithSupplier_supplier (table: clientInSubdivisionWithSupplier)


ALTER TABLE clientInSubdivisionWithSupplier ADD CONSTRAINT clientInSubdivisionWithSupplier_supplier FOREIGN KEY clientInSubdivisionWithSupplier_supplier (supplierId)
    REFERENCES supplier (id);
-- Reference:  clientManagerLink_accountManager (table: clientAccountManager)


ALTER TABLE clientAccountManager ADD CONSTRAINT clientManagerLink_accountManager FOREIGN KEY clientManagerLink_accountManager (accountManagerId)
    REFERENCES accountManager (id);
-- Reference:  clientManagerLink_client (table: clientAccountManager)


ALTER TABLE clientAccountManager ADD CONSTRAINT clientManagerLink_client FOREIGN KEY clientManagerLink_client (clientId)
    REFERENCES client (id);
-- Reference:  clientRebateLink_client (table: clientRebate)


ALTER TABLE clientRebate ADD CONSTRAINT clientRebateLink_client FOREIGN KEY clientRebateLink_client (clientId)
    REFERENCES client (id);
-- Reference:  clientRebateLink_rebate (table: clientRebate)


ALTER TABLE clientRebate ADD CONSTRAINT clientRebateLink_rebate FOREIGN KEY clientRebateLink_rebate (rebateId)
    REFERENCES rebate (id);
-- Reference:  clientReferral_associate (table: clientReferral)


ALTER TABLE clientReferral ADD CONSTRAINT clientReferral_associate FOREIGN KEY clientReferral_associate (associateId)
    REFERENCES associate (id);
-- Reference:  client_area (table: client)


ALTER TABLE client ADD CONSTRAINT client_area FOREIGN KEY client_area (areaId)
    REFERENCES area (id);
-- Reference:  client_clientGroup (table: client)


ALTER TABLE client ADD CONSTRAINT client_clientGroup FOREIGN KEY client_clientGroup (clientGroupId)
    REFERENCES clientGroup (id);
-- Reference:  client_industry (table: client)


ALTER TABLE client ADD CONSTRAINT client_industry FOREIGN KEY client_industry (industryId)
    REFERENCES industry (id);
-- Reference:  client_order (table: purchase)


ALTER TABLE purchase ADD CONSTRAINT client_order FOREIGN KEY client_order (clientId)
    REFERENCES client (id);
-- Reference:  invoiceItem_invoice (table: invoiceItem)


ALTER TABLE invoiceItem ADD CONSTRAINT invoiceItem_invoice FOREIGN KEY invoiceItem_invoice (invoiceId)
    REFERENCES invoice (id);
-- Reference:  invoiceItem_product (table: invoiceItem)


ALTER TABLE invoiceItem ADD CONSTRAINT invoiceItem_product FOREIGN KEY invoiceItem_product (productId)
    REFERENCES product (id);
-- Reference:  invoice_cc (table: invoice)


ALTER TABLE invoice ADD CONSTRAINT invoice_cc FOREIGN KEY invoice_cc (ccId)
    REFERENCES cc (id);
-- Reference:  price_product (table: price)


ALTER TABLE price ADD CONSTRAINT price_product FOREIGN KEY price_product (productId)
    REFERENCES product (id);
-- Reference:  price_sourceType (table: price)


ALTER TABLE price ADD CONSTRAINT price_sourceType FOREIGN KEY price_sourceType (sourceTypeId)
    REFERENCES sourceType (id);
-- Reference:  productCategory_productCategory (table: productCategory)


ALTER TABLE productCategory ADD CONSTRAINT productCategory_productCategory FOREIGN KEY productCategory_productCategory (productCategoryId)
    REFERENCES productCategory (id);
-- Reference:  productDetails_product (table: productDetail)


ALTER TABLE productDetail ADD CONSTRAINT productDetails_product FOREIGN KEY productDetails_product (productId)
    REFERENCES product (id);
-- Reference:  productMatch_invoiceItem (table: productMatch)


ALTER TABLE productMatch ADD CONSTRAINT productMatch_invoiceItem FOREIGN KEY productMatch_invoiceItem (invoiceItemId)
    REFERENCES invoiceItem (id);
-- Reference:  productMatch_price (table: productMatch)


ALTER TABLE productMatch ADD CONSTRAINT productMatch_price FOREIGN KEY productMatch_price (priceId)
    REFERENCES price (id);
-- Reference:  productMatch_product (table: productMatch)


ALTER TABLE productMatch ADD CONSTRAINT productMatch_product FOREIGN KEY productMatch_product (productId)
    REFERENCES product (id);
-- Reference:  product_measureType (table: product)


ALTER TABLE product ADD CONSTRAINT product_measureType FOREIGN KEY product_measureType (measureTypeId)
    REFERENCES measureType (id);
-- Reference:  product_productCategory (table: product)


ALTER TABLE product ADD CONSTRAINT product_productCategory FOREIGN KEY product_productCategory (productCategoryId)
    REFERENCES productCategory (id);
-- Reference:  product_supplier (table: product)


ALTER TABLE product ADD CONSTRAINT product_supplier FOREIGN KEY product_supplier (supplierId)
    REFERENCES supplier (id);
-- Reference:  purchase_item_product (table: purchaseItem)


ALTER TABLE purchaseItem ADD CONSTRAINT purchase_item_product FOREIGN KEY purchase_item_product (productId)
    REFERENCES product (id);
-- Reference:  purchase_purchase_item (table: purchaseItem)


ALTER TABLE purchaseItem ADD CONSTRAINT purchase_purchase_item FOREIGN KEY purchase_purchase_item (purchaseId)
    REFERENCES purchase (purchaseId);
-- Reference:  rebate_product (table: rebate)


ALTER TABLE rebate ADD CONSTRAINT rebate_product FOREIGN KEY rebate_product (productId)
    REFERENCES product (id);
-- Reference:  referral_client (table: clientReferral)


ALTER TABLE clientReferral ADD CONSTRAINT referral_client FOREIGN KEY referral_client (clientId)
    REFERENCES client (id);
-- Reference:  referredBy_associate (table: associateReferredBy)


ALTER TABLE associateReferredBy ADD CONSTRAINT referredBy_associate FOREIGN KEY referredBy_associate (associateId)
    REFERENCES associate (id);
-- Reference:  referredBy_associate2 (table: associateReferredBy)


ALTER TABLE associateReferredBy ADD CONSTRAINT referredBy_associate2 FOREIGN KEY referredBy_associate2 (referredById)
    REFERENCES associate (id);
-- Reference:  strategicAreaLink_client (table: clientStrategicArea)


ALTER TABLE clientStrategicArea ADD CONSTRAINT strategicAreaLink_client FOREIGN KEY strategicAreaLink_client (clientId)
    REFERENCES client (id);
-- Reference:  strategicAreaLink_strategicArea (table: clientStrategicArea)


ALTER TABLE clientStrategicArea ADD CONSTRAINT strategicAreaLink_strategicArea FOREIGN KEY strategicAreaLink_strategicArea (strategicAreaId)
    REFERENCES strategicArea (id);
-- Reference:  supplierAreaLink_area (table: supplierArea)


ALTER TABLE supplierArea ADD CONSTRAINT supplierAreaLink_area FOREIGN KEY supplierAreaLink_area (areaId)
    REFERENCES area (id);
-- Reference:  supplierAreaLink_supplier (table: supplierArea)


ALTER TABLE supplierArea ADD CONSTRAINT supplierAreaLink_supplier FOREIGN KEY supplierAreaLink_supplier (supplierId)
    REFERENCES supplier (id);



-- End of file.
